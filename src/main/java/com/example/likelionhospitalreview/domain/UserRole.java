package com.example.likelionhospitalreview.domain;

public enum UserRole {
    ADMIN, USER;
}
