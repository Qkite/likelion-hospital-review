package com.example.likelionhospitalreview.service;

import com.example.likelionhospitalreview.domain.User;
import com.example.likelionhospitalreview.domain.dto.UserDto;
import com.example.likelionhospitalreview.domain.dto.UserJoinRequest;
import com.example.likelionhospitalreview.exception.ErrorCode;
import com.example.likelionhospitalreview.exception.HospitalReviewAppException;
import com.example.likelionhospitalreview.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

//    @Value("${jwt.token.secret}")
//    private String secreatKey;
//    private long expireTimeMs = 1000*60*60l;

    public UserDto join(UserJoinRequest userJoinRequest){
        //비즈니스 로직 - 회원가입
        //회원 userName(id) 중복 check
        //중복이면 회원가입 x --> Exception(예외) 발생
        userRepository.findByUserName(userJoinRequest.getUserName())
                .ifPresent(user->{
                    throw new HospitalReviewAppException(ErrorCode.DUPLICATED_USER_NAME, String.format("UserName:%s",userJoinRequest.getUserName()));
                });

        //회원가입 .save()
        User savedUser = userRepository.save(userJoinRequest.toEntity(encoder.encode(userJoinRequest.getPassword())));

        return UserDto.builder()
                .id(savedUser.getId())
                .userName(savedUser.getUserName())
                .email(savedUser.getEmail())
                .build();
    }
}
